You can find a lot of free flags here:

https://dribbble.com/shots/1211759-Free-195-Flat-Flags

For supporting a new language just download one, rescale it to 30x18 px and save
it as <lang>.png where <lang> is the language shortcut (e.g. "en").

Whene done make sure that your flag looks good in the drop down and when
selected. The flat black of the flags for instance provides not enough contrast
with the dark Recalbox blue. Just adjust the flags as needed.

