+++
date = "2017-03-04T16:36:09+02:00"
image = "/images/blog/2017-03-04-favicons/favicons_feature-visual.jpg"
title = "Nouvelles icones pour les favoris !"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++
<p dir="auto">Aujourd'hui nous allons vous présenter une petite nouveauté sur les favoris de recalbox 4.1.0.</p><p dir="auto">Le système de favoris est une feature qui a été présenté avec la version 4.0.0 de recalbox et permet de sélectionner des jeux dans vos systèmes avec la touche Y, pour qu'ils apparaissent en haut de la liste mais aussi dans un système particulier, le <strong>Système Favoris</strong>. Mais quand vous avez beaucoup de jeux taggés en favoris, le système favoris peut devenir un peu désordonné.</p><p dir="auto">Nous avons donc décidé de remplacer l'icone standard  <img src="/images/blog/2017-03-04-favicons/star.png" height="20" width="20" align="absmiddle"> partagée par tous les favoris de tous les systèmes par une icone <strong>spécifique pour chaque système</strong>.</p><p dir="auto">Vous pouvez voir sur les screenshots le résultat sur le système GBA et le résultat dans le système favoris :</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-04-favicons/es_favicons_2.jpg" target="_blank"><img src="/images/blog/2017-03-04-favicons/es_favicons_2.jpg" alt="es_favicons_2"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-04-favicons/es_favicons.jpg" target="_blank"><img src="/images/blog/2017-03-04-favicons/es_favicons.jpg" alt="es_favicons"></a></div><p dir="auto">Voici le rendu final en vidéo :</p><iframe width="560" height="315" src="https://www.youtube.com/embed/le9c8dVtf8I" frameborder="0" allowfullscreen=""></iframe>