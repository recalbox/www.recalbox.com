+++
date = "2017-10-13T13:37:00+02:00"
image = "/images/recalbox-4.1-is-out/recalbox-4.1-is-out.jpg"
title = "4.1 SAIU !!!"
slug = "recalbox-4.1-is-out"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Até que enfim ... Após mais de um ano de desenvolvimento, diversos lançamentos públicos instáveis desde Fevereiro com um grande feedback de vocês todos, prezada comunidade, e muitas outras mudanças menos visíveis (novo blog, nova infraestrutura, nova forma de desenvolvimento/construção/lançamento, e muito mais ...)

Estamos felizes em lançar a versão 4.1 do Recalbox ! Estas são algumas das principais funcionalidades desta nova versão:

- Bluetooth foi totalmente refeito (eu QUERO DIZER, totalmente reescrito) para que pudéssemos suportar o bluetooth embutido do Pi3. Note que você *DEVE* parear novamente todos os dispositivos, e isto é algo que não pode ser atualizado.
- Novas placas suportadas: Odroid XU4 e C2, PC X86 (32 e 64 bits) ... assim como o Pi0-w !
- Na parte do EmulationStation : muitas novas traduções, ajuda contextual, novos ícones de favoritos, download de atualizações com progresso em porcentagem, Screenscraper suportado nativamente, seleção de dispositívo de saída de áudio, diversos bugs corrigidos ... e um novo teclado virtual!
- Um novo gerenciador web, graças ao DjLeChuck. Acesse http://recalbox (Windows apenas) ou http://recalbox.local (Linux e MAC)
- Novos drivers para os controles XBox360, DualShock3 + DualShock4 com suporte a bluetooth (nem todas as gerações de controles são compatíveis)
- Novos sistemas : AppleII, DOS, PSP, Dreamcast e ... Commodore64 ! Até mesmo Gamecube e Wii (apenas em PC 64 bits)! Estes sistemas não estão disponíveis em todas as placas. Se você não enxergar uma nova pasta dentro do diretório roms, significa que o sistema não é suportado em sua placa.
- Novo emulador MAME : advancemame 3.4 (por enquanto, apenas disponível no Pi)
- Melhor suporte a telas TFT
- Sem dores de cabeça com formatação FAT32 ! O Recalbox está disponível no formato imagem !

E muitas outras funcionalidades que você descobrirá quando seu Recalbox for atualizado ! Junte-se a nós nos fóruns para celebrar este novo lançamento e compartilhar seu feedback (e bugs ... eventualmente ... espero que não !)

Uma última coisa : seja paciente durante a atualização. O Recalbox irá reiniciar diversas vezes, a tela ficará preta por alguns instantes ... Não se preocupe, está tudo sob controle ! Apenas deixe seu Recalbox trabalhar !

PS: Quer saber mais ? De uma olhada [aqui](https://archive.recalbox.com/v1/upgrade/rpi3/recalbox.changelog?source=install) para o changelog completo (que será apresendado assim que a atualização finalizar), e arquivos .img estão disponíveis na [página de downloads](https://archive.recalbox.com).

E caso você se sinta impaciente durante a atualização e deseja se deleitar com as novas funcionalidades da 4.1 [dê uma olhada na playlist da 4.1 no Youtube](https://www.youtube.com/playlist?list=PL2oNQ0AT7fx2LqCuw9VeK0it9ErFiYlnT).
