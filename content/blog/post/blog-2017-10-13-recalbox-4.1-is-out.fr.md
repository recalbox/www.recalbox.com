+++
date = "2017-10-13T13:37:00+02:00"
image = "/images/recalbox-4.1-is-out/recalbox-4.1-is-out.jpg"
title = "La 4.1 est LA !!!"
slug = "recalbox-4.1-is-out"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"

+++

Que le chemin a été long ... Plus d'un an de développement, une beta publique ouverte mi-Février avec beaucoup de retours de bugs grâce à vous, la communauté, beaucoup de changements pas forcément visibles pour vous (le blog, des éléments d'infrastructure, des évolutions sur le processus de développement, et bien d'autres ...)

Voici donc cette 4.1, et ses quelques nouveautés par rapport à la 4.0 :

- Evolution majeure du bluetooth (c'est peu de le dire ... tout a dû être réécrit) qui supporte enfin le bluetooth du pi3. Il vous faudra obligatoirement réappairer vos pads bluetooth, que vous utilisiez un dongle bluetooth ou préfériez l'interne du Pi3
- Nouvelles architectures supportées : Odroid XU4 et C2, PC X86 (32 et 64 bits) ... ainsi que le Pi0-w !
- Côté EmulationStation : beaucoup de nouvelles traductions, ajout d'une aide contextuelle, nouvelles icônes pour les favoris, indicateur de progression du téléchargement de la mise à jour, ajout du scraper de nos amis de chez Screenscraper, choix du périphérique de sortie audio, corrections de bugs et ... un clavier virtuel !
- Un nouveau webmanager grâce à DjLeChuck. A tester sur http://recalbox (Windows only) ou http://recalbox.local (Linux et MAC)
- Nouveaux drivers XBox360 et DualShock3 + support des pads PS4 en BT (attention, tous les pads ne sont pas compatibles)
- Nouveaux systèmes : AppleII, DOS, PSP, Dreamcast et ... Commodore64 ! Et même la Gamecube et la Wii (mais seulement sur PC 64 bits) Ces systèmes ne sont pas disponibles sur toutes les plateformes. S'il n'y a pas de dossier correspondant dans roms, ce n'est pas disponible pour votre carte.
- Nouvel émulateur MAME : advancemame 3.4 (disponible que sur Pi pour le moment)
- Meilleur support des écrans TFT
- Recalbox est maintenant disponible au format .img : plus besoin de s'ennuyer avec le formatage en FAT32 !

Et beaucoup, beaucoup d'autres évolutions que vous découvrirez dès le premier boot en 4.1 ! Alors célébrez avec nous cette nouvelle version, et n'hésitez pas à venir sur le forum pour faire remonter vos impressions (agréables forcément hein !)

Lors de la mise à jour, soyez patients, et ce pour plusieurs raisons. D'une part, parce que la mise à jour est assez conséquente et va prendre plusieurs reboots. Ensuite, parce que lors de la mise à jour, l'écran peut devenir noir. Pas de panique, c'est normal, laissez votre Recalbox faire, elle gère !

PS : les plus valeureux peuvent aller voir [de ce côté-ci](https://archive.recalbox.com/v1/upgrade/rpi3/recalbox.changelog?source=install) pour la changelog complète (qui de toute façon sera bien visible lors de votre upgrade), télécharger tout le nécessaire sur [LA page de téléchargement pour les .img entre autres](https://archive.recalbox.com).

Et si jamais vous avez du temps à perdre pendant le téléchargement  et que vous voulez vous auto-divulgâcher (spoiler, en Français, on a de la culture et un Larousse ici !) cette 4.1 [alors go la playlist Youtube](https://www.youtube.com/playlist?list=PL2oNQ0AT7fx2LqCuw9VeK0it9ErFiYlnT)