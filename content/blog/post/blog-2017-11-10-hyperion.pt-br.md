+++
date = "2017-11-10T20:00:00+00:00"
image = "/images/blog/2017-11-10-hyperion/recalbox-hyperion-banner.jpg"
title = "Faça seu recalbox brilhar com o Hyperion!"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

**Hyperion** (https://github.com/hyperion-project/hyperion) é um projeto livre e de código aberto para tornar seu Raspberry Pi em um sistema **similar ao Ambilight**!

O Hyperion em conjunto com o recalbox, tornará seu sistema de retrogaming e multimídia uma experiência bela e imersiva!

## I - Descrição

Primeiramente, não estamos falando em utilizar o Hyperion em um pedaço específico do seu recalbox como o **Kodi**.
Você será capaz de usar as funcionalidades do Hyperion em **CADA JOGO RETRÔ** ao jogar no recalbox!

![Hyperion with recalbox](/images/blog/2017-11-10-hyperion/pukerainbow.gif)

Leds RGB irão **estender dinamicamente** a imagem da sua TV (Flat ou CRT) para as paredes.

Que aparência isto tem? Veja com o _Sonic 3_ do Sega Megadrive/Genesis:

[![Sonic Title](/images/blog/2017-11-10-hyperion/sonic1-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic1.jpg)
[![Sonic Plane](/images/blog/2017-11-10-hyperion/sonic2-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic2.jpg)
[![Sonic Stage 1](/images/blog/2017-11-10-hyperion/sonic3-thumb.jpg)](/images/blog/2017-11-10-hyperion/sonic3.jpg)

E em vídeo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/z1QblkdO6bs" frameborder="0" allowfullscreen></iframe>

## II - Hardware

O Hyperion suporta diversas fitas de LED (veja https://hyperion-project.org/wiki/Supported-hardware) e diversos protocolos:

* **SPI**: LPD6803, LPD8806, APA102, WS2812b, e WS2801
* **USB**: AdaLight e SEDU
* **PWM**: WS2812b

Testes foram feitos na **WS2812b** com PWM e SPI. Eu nunca consegui fazer o PWM funcionar corretamente, então iremos descrever como fazer a instalação do seu recalbox/hyperion com a **WS2812b no modo SPI**. Esta seção está faltando da wiki do Hyperion então, iremos explicar como usar aqui (e também atualizar a wiki do Hyperion com estas informações).

Suporte a **WS2812b** com um fio sob SPI foi adicionado por [penfold42](https://github.com/penfold42) neste [commit](https://github.com/hyperion-project/hyperion/commit/a960894d140405e29edb413685e700d2f1714212). Muito Obrigado.

O tutorial que segue faz uso de ferro de solda. Caso você já tenha um, o resto é relativamente barato. Mas se você deseja fazer sem utilizar soldas, o tutorial oficial pode ser encontrado aqui:
https://hyperion-project.org/threads/raspberry-pi-3-mediacenter-hyperion-ambilight-no-soldering.77/


Você precisará de:

* seu recalbox
* uma fita de led WS2812b [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/ws2812b.jpg)
* um resistor de 330 Ohms a ser inserido no data pin
* algo para cortar a fita [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/pince.jpg)
* um ferro de solda
* fios ou conectores para unir as fitas de led [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/connectors-item.jpg)
* um regulador de tensão para alterar a voltagem da data para 5V [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/voltage.jpg), ou um level shifter <i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/shifter.jpg) para reduzir a alimentação do circuito integrado(VCC) dos leds.
* alguns jumpers dupont para conectar os cabos facilmente ao seu Raspberry Pi [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/dupont.jpg)
* uma fonte de alimentação 4amp (min) 5v [<i class="fa fa-picture-o"></i>](/images/blog/2017-11-10-hyperion/powersupply.jpg)

Não hesite em [visitar a <img style="vertical-align: baseline;" src="/images/shared/recalstore.png"/>](https://www.recalbox.com/shop) para encontrar tudo o que você precisa.


E caso precise economizar dinheiro você pode:

* soldar tudo diretamente (não precisando dos conectores e dupont)
* Usar uma fonte de alimentação 3amp. Eu testei 220 leds com hyperion em uma Aukru 5V 3AM, e funciona relativamente bem, mas quando todos os leds propagam a cor branca, o final da fita emite uma cor amarelada...


## III - Conectando

#### Leds

Calcule as bordas da sua TV e corte uma fita por borda de acordo com seu tamanho.

Solde as fitas entre si OU solde os conectores nas fitas. Siga as setas que aparecem nas fitas para saber qual conector utilizar (IN e OUT).

A aparência é esta:

[![connectors](/images/blog/2017-11-10-hyperion/connectors-thumb.jpg)](/images/blog/2017-11-10-hyperion/connectors.jpg)
[![connectors](/images/blog/2017-11-10-hyperion/solder-thumb.jpg)](/images/blog/2017-11-10-hyperion/solder.jpg)



#### O sistema

Para permitir o RPi enviar dados ao LED, você precisa escolher entre:

* Reduzir o VCC do LED para 4.7V com um regulador de tensão
* alternar o sinal da GPIO para 5V com um level shifter

###### A - Regulador de Tensão

[![Circuit voltage regulator](/images/blog/2017-11-10-hyperion/wiring-regulator.png)](/images/blog/2017-11-10-hyperion/wiring-regulator.png)

(imagem original da [wiki do hyperion](https://hyperion-project.org/wiki/3-Wire-PWM))

Se você estiver no RPI2 ou 3, há uma GPIO maior mas a o pinout é o mesmo.

O RPi é conectado a sua própria fonte de alimentação, e no ground da fonte de alimentação do LED.

O RPi emite dados através da SPI MOSI GPIO para o cabo de dados do LED.

O regulador de tensão é conectado entre a fonte de alimentação do LED e o LED em si.

###### B - Level shifter para gpio (não testado)


Caso você deseje regular a voltagem da GPI, utilize o esquema acima apenas removendo o regulador de tensão e conectando o level shifter entre a GPIO MOSI e o cabo de dados (data) da fita de LED.


## IV - Configuração

#### Hypercon

**Hypercon** é uma ferramenta gráfica de configuração que permitirá a configuração da instalação do Hyperion, e configuração do Hyperion em si.

Baixe e execute o **Hypercon** seguindo o tutorial oficial: https://hyperion-project.org/wiki/HyperCon-Information

[![hypercon screenshot](/images/blog/2017-11-10-hyperion/hypercon-thumb.jpg)](/images/blog/2017-11-10-hyperion/hypercon.jpg)


Você encontrará recursos relacionados a configuração do Hypercon na wiki do Hyperion: https://hyperion-project.org/wiki/HyperCon-Guide

Estas são as instruções de como configurar o Hypercon para o seu recalbox e a **WS2812b no modo spi**(em inglês):

* **Hardware**
 * Type -> WS281X-SPI
 * RGB Byte Order -> GRB
 * Construction and Image Process -> configure dependendo da sua instalação
 * Blackborder Detection -> Enabled
     * Threshold -> 5
     * mode -> osd
* **Process**
  * _Smoothing_
     * Enabled -> True
     * Time -> 200ms
     * Update Freq -> 20
     * Update Delay -> 0
* **Grabber**
  * _Internal Frame Grabber_
     * Enabled -> True
     * Width -> 64
     * Height -> 64
     * Interval -> 200
     * Priority Channel -> 890
  * _GrabberV4L2_
     * Enabled -> False
* External
  * Booteffect / Static Color -> configure o efeito de boot que desejar aqui!


Então clique no botão **Create Hyperion Configuration** e salve o arquivo json com o nome `hyperion.config.json`.

Copie este arquivo para o seu recalbox (através de ssh, ou rede compartilhada) no caminho `/recalbox/share/system/config/hyperion/hyperion.config.json` (`config/hyperion/hyperion.config.json` na rede)


#### Habilite o serviço do hyperion em recalbox.conf

Apenas habilite o serviço no arquivo [recalbox.conf](https://github.com/recalbox/recalbox-os/wiki/recalbox.conf-%28EN%29):  
Altere a linha `hyperion.enabled=0` para `hyperion.enabled=1` (e remova o `;`)

Edite o arquivo config.txt localizado na partição boot e adicione `dtparam=spi=on`:

```bash
mount -o remount,rw /boot && echo 'dtparam=spi=on' >> /boot/config.txt
```

E reinicie.


## V - Aproveite o arco-íris

Está feito! Você criou um ambiente funcional e similar ao Ambilight! Teste alguns jogos retrô, filmes e animes no Kodi, e deixe que a beleza das cores aprimore sua experiência multimídia!

![](/images/blog/2017-11-10-hyperion/nyan.jpg)
