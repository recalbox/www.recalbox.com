+++
date = "2017-11-10T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.10/title-17.11.10.png"
title = "Recalbox 17.11.10"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"

[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
+++

Amis Recalboxiens !

On garde le rythme soutenu de nouvelles versions avec cette **17.11.10** !

Voyons tout de suite les nouveautés qui la composent :

* Si vous êtes attentifs à notre actualité, vous avez sûrement vu notre annocne sur **Hyperion** qui transforme votre Recalbox en un système type Ambilight si tant est que vous ayez le matériel adéquat ! Plus de détails sur [notre post dédié au sujet]({{< ref "/blog/post/blog-2017-11-10-hyperion.fr.md" >}})
* Ajout du Controlblock de Petrock pour qu'il gère nativement les 2 joueurs. Merci [@petrockblog](https://twitter.com/petrockblog)!
* Vous pouvez à présent suffixer vos répertoires scummvm par un .scummvm ! Une fois le renommage effectué, plus besoin de naviguer dans le sous-répertoire pour lancer le jeu. Et vous pouvez même scraper le répertoire pour un affichage direct dans EmulationStation ! N'oubliez pas de garder le .scummvm dans le répertoire il est indispensable. Un exemple pour résumer : renommez votre dossier de Monkey Island 2 en `monkey2.scummvm`, scrapez, et jouez ! Merci [lmerckx](https://gitlab.com/lmerckx).
* Ajout de nouveaux pads préconfigurés : Nintendo Wii U Pro Controller et Switch Pro Controller, 8bitdo FC30 Arcade (BT et USB), Thrustmaster T Mini Wireless et Orange Controller
* Ajout d'un nouveau core lynx (libretro-handy) qui devient celui par défaut. Ce core devrait être plus permissif sur les roms.

Dernier point : nous avons fait à nouveau de gros travaux internes sur le code source de Recalbox. C'est un **grand pas** qui simplifiera énormément les mises à jour du Linux sur lequel Recalbox est construit. La prochaine étape de ces changements sera bientôt disponible pour tests publics. Et enfin 3e étape, on pourra attaquer la vraie mise à jour du Linux de Recalbox. Et ... euh ... vous ai-je dit qu'à l'issue de cette 3e étape, on passe sur Kodi 17 ?


Comme promis, on fait des mises à jour bien plus fréquentes, pas vrai ? ;)
