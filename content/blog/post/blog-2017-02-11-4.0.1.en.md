+++
date = "2017-02-18T14:43:41+02:00"
image = "/images/blog/2017-02-11-4.0.1/4.0.1_release-visual.jpg"
title = "Recalbox 4.0.1 released!"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

<div>We are proud to annouce the release of recalboxOS <b>4.0.1</b>!<br><br>This new version brings the support of <b>Raspberry Pi 2 v1.2</b> and prepare your recalbox for the new 4.1.0 unstable release coming <b>really</b> soon!<br> <br>Update your recalbox as soon as possible, and we promise to be back in no time for the next news :)</div>