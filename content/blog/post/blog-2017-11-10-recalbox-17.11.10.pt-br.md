+++
date = "2017-11-10T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.10/title-17.11.10.png"
title = "Recalbox 17.11.10"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Prezados Recalboxers !

Estamos mantendo a taxa de lançamentos um tanto quanto alta com a nova versão **17.11.10** do Recalbox !

Vamos entrar nos detalhes das novas funcionalidades :

* Se você nos segue, provavelmente viu o tease do **Hyperion**, que transforma seu Recalbox em um sistema similar ao ambilight se você tiver o hardware certo ! Veja mais detalhes nesta [postagem dedicada]({{< relref "blog-2017-11-10-hyperion.pt-br.md" >}}) 
* Adicionado suporte a 2 jogadores no Petrock controlblock. Obrigado [@petrockblog](https://twitter.com/petrockblog)!
* Você pode adicionar o sufixo .scummvm aos diretórios de roms dentro do diretório scummvm ! Ao fazer isto, você não precisará navegar no subdiretório para iniciar o jogo. Você até pode fazer um scrap para obter um visual melhor ! Lembre-se que ainda é necessário o arquivo .scummvm dentro do diretório. Por exemplo: nomeie seu diretório do Monkey Island 2 `monkey2.scummvm`, execute o scrape e jogue ! Obrigado [lmerckx](https://gitlab.com/lmerckx).  
* Novos controles configurados automaticamente: Nintendo Wii U Pro Controller e Switch Pro Controller, 8bitdo FC30 Arcade (BT e USB), Thrustmaster T Mini Wireles e Orange Controller
* Adicionado novo core Lynx(libretro-handy) por padrão. Este novo core deverá ser menos exato com romsets !

E como nota adicional, nós fizemos uma grande reestruturação no repositório do Recalbox. Este é um **grande passo** em direção a uma atualização geral do Linux que construímos. A segunda fase está quase pronta e será aberta para testadores logo, e a terceira fase será uma grande atualização ! Oh, por acaso esqueci de avisar que esta terceira fase tratá o Kodi 17 ?


Mantemos nossa palavra com os novos lançamentos, não mantemos ? ;)
