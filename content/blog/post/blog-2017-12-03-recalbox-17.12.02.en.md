+++
date = "2017-12-03T11:00:00+02:00"
image = "/images/blog/2017-12-03-recalbox-17-12-02/recalbox-17.12.02-banner.jpg"
title = "Recalbox 17.12.02"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  twitter = "https://twitter.com/digitalumberjak"
  name = "digitalLumberjack"

+++

Hi!

We released the `17.12.01` version last Friday and some people detected some lags while browsing games and even while playing.

This was due to the steam controller service that was always started on boot, even if you don't have a steam controller.

We fixed that behavior, and you can now upgrade your recalbox to `17.12.02`.

For a fresh install, download recalbox images on [archive.recalbox.com](https://archive.recalbox.com)