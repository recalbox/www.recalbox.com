+++
date = "2019-11-15T07:00:46Z"
title = "Recalbox 6.1.1 is now available"
image = "/images/blog/2019-11-15-recalbox-6.1.1-dragonblaze-released/Software_Box_Mockup_1_RECALBOX.jpg"

[author]
  name = "OyyoDams"
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  facebook = "https://www.facebook.com/oyyodams/"
  twitter = "https://twitter.com/OyyoDams"
[translator]
  name = "Mouse's Mistake"
  facebook = "https://www.facebook.com/mousemistake/"
  twitter= "https://twitter.com/mousesmistake"
+++

Recalbox keeps getting better !

Since version 6.0 came out, you never stopped thanking us, but you also never stopped reporting on it in order to improve the Recalbox project a little more each and every day. That's the summary of all the latest changes !


## Recalbox 6.1: DragonBlaze

Version 6.0 was a revolution but as we always want to do more, this new version called "6.1" brings along its fair share of new things and brings to total amount of emulated systems supported by Recalbox to near 95, adding in more or less 15 new machines ! Including, Uzeboxes, Amstrad GX4000, Apple IIGS, Amiga 600 & 1200, Spectravideo, Sharp X1, Palm, PC-88, TIC-80, Thomson MO6, Olivetti Prodest PC128, MSXturboR, Multivision, but also the much anticipated arcade systems Atomiswave, NAOMI and even the Saturn* (*Only available on PC at the time being).

Recalbox 6.1 also brings in the acclaimed video snaps feature, small video cuts that play automatically when you stay on a game in the list. This feature is very useful to get the players to discover new titles and to have an immediate guess of what the gameplay is like !

Recalbox 6.1 comes with the return of the internal scraper (a program that scraps all the meta-data of a video game like its cover, its info, its description...), which also comes with new options !

In addition to the Raspberry Pi Zero-W-H, 1, 2, 3B, 3B+ and Compute Module 3, Odroid C2, XU4 and PPC x86/x64 family, Recalbox 6.1 can now be installed on the new Raspberry Pi 3A+, good news for the makers !

The result of a long development and drastic optimization period, the GPI Case is now 100% natively supported on Recalbox 6.1. No need to install scripts or to do any kind of configuration, everything is automatic and fully optimized to make the most out of the modest capabilities of the Raspberry Pi 0/0W ! For that to even happen, Recalbox 6.1 went through an intense lighten cure and near-military exercise !

The best in all this ? EVERY SINGLE CARD is profiting from improvements and optimizations made for the GPI Case. On numerous cards, the boot time as been reduced to almost-instant, and the system globally improved in performance, reactiveness and freshness... and where... who can do the least, can do the most !

In no particular order :

    • RetroArch has been updated to version 1.7.8, finally letting go of the atrocious green RGUI, which is replaced with the all new and sober OZONE !
    • ... and even introduces the beginnings of real-time retro game translations, with the "text-to-speech"* feature, giving for the first time ever A VOICE to retro games ! (*requires some configuration)
    • Configuring Bluetooth game pads is now easier than ever
    • A lot more emulators now support opening ".7z" files
    • Improvements to the audio balancing between the games and the Recalbox menu
    • FinalBurn Alpha becomes FinalBurn Neo, following the project's demise. (Romset 0.2.97.44 still recommended, that doesn't change)
    • MAME evolves, and now becomes "mame2003_plus". While completely backwards compatible with the Mame 0.78 romset that has been used until now, we now recommend getting on the "2003+" romset, adding compatibility for more than 500 new arcade games !
    • A revamped DEMO mode, with a lot of new settings like system picking but also the addition of an end screen to remind the player of the title and the system of the game he just played, if he wishes to play it again !


## Recalbox 6.1.1: DragonBlaze

Let's be honest, don't expect any major things to come out of Recalbox 6.1.1.
With all the changes that were brought along with version 6.1, and despite our (very) numerous tests, you reported instabilities and bugs.
That's why we focused on bug fixing in order to forever stabilize Recalbox 6.1 with this bugfix called "6.1.1", before getting some new surprises out with Recalbox 6.2 : but that's another story, please be patient !

There, we entirely reworked the Wi-Fi and Bluetooth support.
Connecting new wireless controllers is easier and more stable than ever !

We fixed a whole lot of small bugs on XU4, PC, but also the usage of some emulators like SNES, GX400, Apple II, Atari...

We also took advantage of the situation to update numerous emulator cores to let you make use of the latest features, and a lot of internal improvements.


## Download

Want to try Recalbox 6.1.1 DragonBlaze ?

[Download it](https://archive.recalbox.com) ! It's free, open-source, handeled by a benevolent support team and boosted by an army of volunteers in a non-lucrative goal (just for fun !)

-----
