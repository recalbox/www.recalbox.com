+++
date = "2018-01-30T11:00:00+02:00"
image = "/images/blog/2018-01-30-panasonic-3do/recalbox-panasonic-3do-banner.png"
title = "Panasonic 3DO"

[author]
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  name = "OyyoDams"
+++

Hi friends!

We are happy to present you a new often unrecognized console and yet with outstanding performances at its release: the **Panasonic 3DO!**

Unfortunately, this new system will only be available for Odroid XU4 and PC X86 / X86_64 versions of Recalbox. Indeed, on Raspberry PI, even the 3, we don't get a satisfactory emulation and that even using the lowest settings + the activation of a frameskip. As a result, we have decided not to activate this system in other versions of Recalbox.

In order to emulate this system we'll use the [core libretro 4do](https://github.com/libretro/4do-libretro) core based on [4DO](http://www.freedo.org/) emulator.

For your information, this core needs to add some new bios. Here is the file to [add in your Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) : 

- **51f2f43ae2f3508a14d9f56597e2d3ce** - **panafz10.bin**

We hope you'll enjoy this new core and (re)discover the Panasonic 3DO games library.

## A video review of the 30 best Panasonic 3DO games

<iframe width="560" height="315" src="https://www.youtube.com/embed/un6u1nmvyTo" frameborder="0" allowfullscreen></iframe>
