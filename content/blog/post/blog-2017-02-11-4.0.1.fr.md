+++
date = "2017-02-18T14:43:41+02:00"
image = "/images/blog/2017-02-11-4.0.1/4.0.1_release-visual.jpg"
title = "Recalbox 4.0.1 est disponible !"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

<div>Nous sommes fiers de vous annoncer la sortie de recalboxOS <b>4.0.1</b>!<br><br>Cette nouvelle version apporte le support du <b>Raspberry Pi 2 v1.2</b> et prépare vôtre Recalbox pour la mise à jour vers recalbox 4.1.0 dont la version unstable arrive <b>très</b> bientôt !<br> <br>Mettez à jour vôtre recalbox des que possible, et restez à l'écoute car nous revenons très vite pour une autre news ! :)</div>