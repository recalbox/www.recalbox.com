+++
date = "2017-11-17T10:30:00+02:00"
image = "/images/blog/2017-11-17-bump-emulators/recalbox-bump-emulators-banner.png"
title = "Atualização dos Emuladores"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Caros amigos Recalboxers !

Junto com esta nova versão, é hora de atualizar os emuladores incluidos no Recalbox !

Vamos entrar nos detalhes das correções e funcionalidades adicionadas: 


- **RetroArch** : 

  - Atualizada para versão **1.6.9**, que inclui diversas melhorias e correções. Se você deseja mais informações acesse esta [postagem da libretro](https://www.libretro.com/index.php/retroarch-1-6-9-released/).

- **ScummVM** :

  - Atualizado para a versão **1.10.0** "WIP". Esta versão funciona em todas as placas Recalbox, incluindo Odroid XU4. Também corrige diversos problemas de analógicos de joystick e mouses com lag ou imprecisos. Para mais informações, este é o [changelog](https://github.com/scummvm/scummvm/blob/master/NEWS#L4) da ScummVM 1.10.0.

- **DosBox** :

  - Atualizado para a versão **0.74 r4063**. Obrigado [lmerckx](https://gitlab.com/lmerckx)

- **Libretro cores** :

  - **libretro-81**
  - **libretro-beetle-lynx**
  - **libretro-beetle-ngp**
  - **libretro-beetle-pce**: adicionado suporte a imagens MAME CHD
  - **libretro-beetle-supergrafx**: adicionado suporte ao RetroAchievements, turbo on/off para modo de controle 2 botões, suporte a imagens MAME CHD
  - **libretro-beetle-vb**
  - **libretro-beetle-wswan**: corrigido problema de rotação da tela
  - **libretro-bluemsx**: adicionado suporte a arquivos CAS e M3U
  - **libretro-cap32**
  - **libretro-catsfc**: corrigidos problemas com som, emulação de CPU, correção de lag, correções e melhorias de BSX e SuperFX
  - **libretro-fba**: atualizado para a última versão, v0.2.97.42. Novos arquivos DAT disponíveis [aqui](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro)
  - **libretro-fceumm**: diversas atualizações e correções para o mapper code
  - **libretro-fmsx**: adicionado suporte a arquivos .dsk/.cas e opções de core nospritelimit e crop overscan, corrigidos problemas de multidisk (múltiplos discos)
  - **libretro-fuse**
  - **libretro-gambatte**: corrigidos problemas com a paleta de cores
  - **libretro-genesisplusgx**: adicionado core com ciclo de som preciso, suporte a overclock e possibilidade de remover limites per-line sprite, corrigida a inicialização de buffer de hardware de CD ao utilizar alocação dinâmica de memória
  - **libretro-glupen64**: adicionado suporte a alta resolução
  - **libretro-gpsp**
  - **libretro-gw**: corrigindo problemas com high scores não atualizando devidamente
  - **libretro-hatari**
  - **libretro-imame**: diversas correções e melhorias
  - **libretro-lutro**: diversas correções e melhorias
  - **libretro-mame2003**:  corrigidos problemas de som DCS (Mortal Kombat 1/2/3/Ultimate, NBA Jam, Total Carnage, etc. outros jogos.), melhorias de compatibilidade de jogos, suporte a C-based MIPS3 (Killer Instinct e Killer Instinct 2 funcionando em X86/X86_64 apenas)
  - **libretro-meteor**
  - **libretro-mgba**: atualizado para versão corrente 0.6.1 , diversas melhorias
  - **libretro-nestopia**: suporte a overclock 2x
  - **libretro-nxengine**: atualizado para v1.0.0.6
  - **libretro-o2em**
  - **libretro-pcsx**: corrigidos problemas de polígonos, adicionada opção de core dithering habilitado/desabilitado
  - **plibretro-picodrive**: diversas melhorias de precisão, opção de overlock do 68k
  - **libretro-pocketsnes**
  - **libretro-prboom**: diversas correções para cursor, controles, savegames, suporte a mouse/teclado
  - **libretro-prosystem**: corrigida a database e paleta de cores
  - **libretro-quicknes**
  - **libretro-snes9x-next**: corrigido acesso VRAM inválido, speedhack para SuperFX
  - **libretro-snes9x**: corrigido MSU1 (problema de ruído no som), correção para o chrono trigger, corrigida proporção de tela(aspect ratio) e adicionada opção de overclock do Super FX 20MHz
  - **libretro-stella**
  - **libretro-tgbdual**
  - **libretro-vecx**: atualizado line drawing e adicionada opção de core de multiplicador de resolução de tela
  - **libretro-vice**: melhorias no redimensionamento de tela para regiões PAL/NTSC

- **libretro-mame2003 - exemplos de novos jogos suportados** :

[![cute_fighter](/images/blog/2017-11-17-bump-emulators/cutefght-thumb.png)](/images/blog/2017-11-17-bump-emulators/cutefght.png)
[![dj_boy](/images/blog/2017-11-17-bump-emulators/djboy-thumb.png)](/images/blog/2017-11-17-bump-emulators/djboy.png)
[![dreamworld](/images/blog/2017-11-17-bump-emulators/dreamwld-thumb.png)](/images/blog/2017-11-17-bump-emulators/dreamwld.png)
[![vasara2](/images/blog/2017-11-17-bump-emulators/vasara2-thumb.png)](/images/blog/2017-11-17-bump-emulators/vasara2.png)
   
[![gaia_the_last_choice_of_zarth](/images/blog/2017-11-17-bump-emulators/gaialast-thumb.png)](/images/blog/2017-11-17-bump-emulators/gaialast.png)
[![gogo_mile_smile](/images/blog/2017-11-17-bump-emulators/gogomile-thumb.png)](/images/blog/2017-11-17-bump-emulators/gogomile.png)
[![rolling_crush](/images/blog/2017-11-17-bump-emulators/rolcrush-thumb.png)](/images/blog/2017-11-17-bump-emulators/rolcrush.png)
[![regulus](/images/blog/2017-11-17-bump-emulators/regulus-thumb.png)](/images/blog/2017-11-17-bump-emulators/regulus.png)
   

Então, com este último lançamento atualizamos quase todos os emuladores incluídos no Recalbox.
Os que ainda faltam (advancemame, dolphin, moonlight, mupen64plus, ppsspp) serão atualizados num próximo lançamento.

Esperamos que vocês gostem. Agradecemos ao [Time Libretro](https://github.com/orgs/libretro/people), [Time ScummVM](https://github.com/orgs/scummvm/people), e todas as pessoas trabalhando nestes emuladores.

Saudações,
Time Recalbox.
