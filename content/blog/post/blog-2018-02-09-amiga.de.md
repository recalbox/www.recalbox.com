+++
date = "2018-02-09T11:00:00+00:00"
image = "/images/blog/2018-02-09-amiga/recalbox-amiga-banner.png"
title = "Amiga"

[author]
  github = "https://github.com/voljega"
  gitlab = "https://gitlab.com/voljega"
  name = "Voljega"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Hallo zusammen!

Nach einer langen Test-Periode, gibt es nun endlich eine Amiga-Emulation in Recalbox!

Hier die Details zur Funktionalität:
* Der genutzte Emulator ist [Amiberry Version 2.1](https://github.com/midwan/amiberry).
* Derzeit werden 2 Systeme unterstützt: Amiga 600 und Amiga 1200. Der Amiga CD32 Emulator muss bei Amiberry noch etwas ausreifen (zukünftige Version 2.5) und wird dann später in Recalbox integriert.
* Unterstützte Formate sind Disketten-Images (.adf, automatisches Laden mehrerer Disketten) und Festplatten (WHDL).


Es wird mindestens eine BIOS-Datei benötigt, je nach dem, welches System emuliert werden soll:

* Amiga 600 ADF  **82a21c1890cae844b3df741f2762d48d  kick13.rom**
* Amiga 600 WHDL **dc10d7bdd1b6f450773dfb558477c230  kick20.rom**
* Amiga 1200 (ADF und WHDL) **646773759326fbac3b2311fd8c8793ee  kick31.rom**

Weitere Infos gibt es in der readme.txt im entsprechenden ROM-Verzeichnis und auf der passenden [Wiki-Seite](https://github.com/recalbox/recalbox-os/wiki/Amiga-on-Recalbox-%28EN%29) (englisch).

Tausend Dank an Ironic und Wulfman für ihr großes Engagement beim Testen und Helfen!

Hier noch ein kleines Video, um euch einige der besten Spiele in Erinnerung zu rufen:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SrO9iJp8e2g" frameborder="0" allowfullscreen></iframe>