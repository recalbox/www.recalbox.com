+++
date = "2017-02-20T16:09:05+02:00"
image = "/images/blog/2017-02-20-osk/recalbox-blog-osk-title.jpg"
title = "On Screen Keyboard (OSK)"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

<div><p dir="auto">The new recalbox version will be more friendly than ever with you.</p><p dir="auto">Who has never had the annoying case of looking for an USB keyboard to input WIFI settings, a game information in metadatas, etc... ?</p><p dir="auto">Today, with this new release, this time is over. </p><p dir="auto">You'll be able to use your gamepad and the new <strong>On Screen Keyboard (OSK)</strong> in EmulationStation.</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-20-osk/recalbox-osk.jpg" target="_blank"><img src="/images/blog/2017-02-20-osk/recalbox-osk.jpg" alt="es_osk"></a></div><p dir="auto">Here is a short demo video:</p><div style="text-align: center"><iframe width="560" height="315" src="https://www.youtube.com/embed/LeWHVSflngU" frameborder="0" allowfullscreen="">video</iframe></div></div>