+++
date = "2018-03-16T01:00:00+02:00"
image = "/images/blog/2018-03-16-recalbox-18-03-16/recalbox-18.03.16-banner.jpg"
title = "Recalbox 18.03.16"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Bonjour chers retrojoueurs !

J'espère que la dernière version vous a plu et que vous avez bien profité de l'Amiga sur vos Pi !

Le printemps Recalbox arrive un peu en avance cette année avec quelques nouveautés que vous ne voudriez manquer pour aucun prétexte ! C'est parti pour la changelog :

* ES : un nouveau mode carousel ainsi que de nombreuses nouvelles options pour les thèmes ! Faites un tour du côté des Options de l'interface > Configuration du thème

* ES : fini les écrans tout blanc quand la VRAM sature ! Les créateurs de thèmes vont pouvoir se lâcher sur des fonds en 1080p

* segacd : ajout du support des chd

* son : application du volume uniquement pour les périphériques de sortie. Ca devrait corriger les problèmes de larsen pour les utilisateurs en x86

* psx : correction d'une faute de frappe (extension mu3 au lieu de m3u)

* Moonlight : mise à jour en 2.4.6 qui apporte le support de GFE 3.12 + possibilité de gérer plusieurs PC GFE du réseau. Donc oui, vous pourrez enfin mettre à jour GFE sur vos PC ! Et le scrape a aussi évolué : on essaie avant tout de récupérer la jaquette de GFE, puis on se penche sur TGDB pour le reste.

* recalbox.conf : ajout d'une option `auto` comme mode video pour aider les gens avec des écrans particuliers. Voici comment ça marche : Recalbox vérifiera si l'écran gère le 720p et switchera dessus le cas échéant. Sinon, on reste sur la résolution par défaut. L'option n'est pas active par défaut pout le moment.
 
* Pi : plutôt que de forcer le son en HDMI, on le laisse ipar défaut en autodétection. Ca se combine plutôt bien avec le mode auto décrit juste au-dessus.

* ScummVM : activation de l'émulation MT-32 pour des musiques plus sympa sur les versions PC des jeux

* DosBox : mise à jour en r4076 + ajout d'un clavier virtuel sur lequel vous pouvez assigner votre gamepad ! Des détails [ici](https://gitlab.com/recalbox/recalbox/issues/363). Pour les moins courageux : appuyez sur CTRL+F1

* Correction de bugs sur FBA-LIBRETRO
 
* Petite correction pour MAME pour l'affichage des noms par défaut

* PPSSPP mis à jour en v1.5.4

* La video d'intro est revenue à celle d'origine + quelques nouvelles que vous devriez apprécier ;) Elles sont choisies aléatoirement au boot

* Correction d'une faute sur x68000 pour la gestion multidisquettes par .m3u

* Vous pouvez à présent démarrer des roms depuis le web manager

* Ajout du mot clé MUSIC pour les partages réseau dans /boot/recalbox-boot.conf

* ES: les sous-répertoires de musiques sont maintenant scannés, correction de quelques bugs au démarrage de ES, Kodi ne peut être lancé que depuis la vue des systèmes, Liste de choix pour les SSID WiFi, correction d'un écran noir occasionnel au retour des émulateurs

Amusez-vous bien avec cette nouvelle version, et faites-nous vos retours sur le forum !
