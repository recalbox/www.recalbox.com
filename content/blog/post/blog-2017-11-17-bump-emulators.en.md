+++
date = "2017-11-17T10:30:00+02:00"
image = "/images/blog/2017-11-17-bump-emulators/recalbox-bump-emulators-banner.png"
title = "Emulators Update"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++

Dear fellow Recalboxers !

With this new release, it is time to bump/update emulators included in Recalbox !

Let's get in the depths of the majors bugfixs / new features :
   
   
- **RetroArch** : 

  - Updated on **1.6.9** version, that includes many fixes and improvements. If you want more informations, go on [libretro release post](https://www.libretro.com/index.php/retroarch-1-6-9-released/).

- **ScummVM** :

  - Updated on **1.10.0** "WIP" release. This version now work on all Recalbox boards, including Odroid XU4. It fixes many issues as the jerky/laggy analog joystick mouse control problem. For more information here is the 1.10.0 ScummVM [changelog](https://github.com/scummvm/scummvm/blob/master/NEWS#L4).

- **DosBox** :

  - Updated on **0.74 r4063**. Thanks [lmerckx](https://gitlab.com/lmerckx)

- **Libretro cores** :

  - **libretro-81**
  - **libretro-beetle-lynx**
  - **libretro-beetle-ngp**
  - **libretro-beetle-pce**: added MAME CHD image support
  - **libretro-beetle-supergrafx**: added support for RetroAchievements, turbo on/off for 2-button controller mode, MAME CHD image support
  - **libretro-beetle-vb**
  - **libretro-beetle-wswan**: fixed a screen rotation issue
  - **libretro-bluemsx**: added CAS and M3U files support
  - **libretro-cap32**
  - **libretro-catsfc**: fixed sound issues, CPU emulation, lagfix, BSX fixes and SuperFX fixes & improvements
  - **libretro-fba**: bumped on last v0.2.97.42. New DAT files are availables [here](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro)
  - **libretro-fceumm**: many fixes and updates for the mapper code
  - **libretro-fmsx**: added support for .dsk/.cas files and nospritelimit & crop overscan core options, fixed some multidisk issues
  - **libretro-fuse**
  - **libretro-gambatte**: fixed color palette bugs
  - **libretro-genesisplusgx**: added a cycle-accurate sound core, support for overclocking and the ability to remove per-line sprite limits, fixed CD hardware buffer initialization when using dynamic memory allocation
  - **libretro-glupen64**: added high resolution support
  - **libretro-gpsp**
  - **libretro-gw**: fixed some issues with high scores not updating properly
  - **libretro-hatari**
  - **libretro-imame**: many fixes  and improvements
  - **libretro-lutro**: many fixes  and improvements
  - **libretro-mame2003**: fixed DCS sound issues fixed (Mortal Kombat 1/2/3/Ultimate, NBA Jam, Total Carnage, etc. other games.), enhanced games compatibility,  backported C-based MIPS3 support (Killer Instinct and Killer Instinct 2 working on X86/X86_64 only)
  - **libretro-meteor**
  - **libretro-mgba**: bumped to upstream 0.6.1 release, many improvements
  - **libretro-nestopia**: added support for 2x overclocking
  - **libretro-nxengine**: bumped to v1.0.0.6
  - **libretro-o2em**
  - **libretro-pcsx**: fixed some polygon issues, added an enable/disable dithering core option
  - **plibretro-picodrive**: many accuracy improvements, added 68k overclocking
  - **libretro-pocketsnes**
  - **libretro-prboom**: many fixes about cursor, gamepads, savegames, mouse/keyboard support
  - **libretro-prosystem**: fixed database and color palette
  - **libretro-quicknes**
  - **libretro-snes9x-next**: fixed invalid VRAM access, speedhack for SuperFX
  - **libretro-snes9x**: fixed MSU1 (crackling sound issue), bugfix on chrono trigger, fixed aspect ratio and added 20MHz Super FX Overclock Option
  - **libretro-stella**
  - **libretro-tgbdual**
  - **libretro-vecx**: updated line drawing and added internal resolution multiplier core option
  - **libretro-vice**: improved screen resizing and handling for PAL/NTSC regions

- **libretro-mame2003 - examples of new supported games** :

[![cute_fighter](/images/blog/2017-11-17-bump-emulators/cutefght-thumb.png)](/images/blog/2017-11-17-bump-emulators/cutefght.png)
[![dj_boy](/images/blog/2017-11-17-bump-emulators/djboy-thumb.png)](/images/blog/2017-11-17-bump-emulators/djboy.png)
[![dreamworld](/images/blog/2017-11-17-bump-emulators/dreamwld-thumb.png)](/images/blog/2017-11-17-bump-emulators/dreamwld.png)
[![vasara2](/images/blog/2017-11-17-bump-emulators/vasara2-thumb.png)](/images/blog/2017-11-17-bump-emulators/vasara2.png)
   
[![gaia_the_last_choice_of_zarth](/images/blog/2017-11-17-bump-emulators/gaialast-thumb.png)](/images/blog/2017-11-17-bump-emulators/gaialast.png)
[![gogo_mile_smile](/images/blog/2017-11-17-bump-emulators/gogomile-thumb.png)](/images/blog/2017-11-17-bump-emulators/gogomile.png)
[![rolling_crush](/images/blog/2017-11-17-bump-emulators/rolcrush-thumb.png)](/images/blog/2017-11-17-bump-emulators/rolcrush.png)
[![regulus](/images/blog/2017-11-17-bump-emulators/regulus-thumb.png)](/images/blog/2017-11-17-bump-emulators/regulus.png)
   

So with this new release, we updated almost all emulators included in Recalbox.
The missing ones (advancemame, dolphin, moonlight, mupen64plus, ppsspp) will be updated, later, in a next release.

We hope you'll enjoy it. We thank the [Libretro's Team](https://github.com/orgs/libretro/people), [ScummVM's Team](https://github.com/orgs/scummvm/people), and all people working on these emulators. 

Best regards,
Recalbox's Team.
