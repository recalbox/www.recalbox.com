+++
date = "2018-03-16"
image = "/images/blog/2018-03-16-emulationstation-theming/18.03.16_release-stable-visual.jpg"
title = "EmulationStation Theming"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"

+++


Hello everyone!

Now that we have a reliable and up to date system, let's bring in some nice cosmetic enhancements!

The Recalbox team is proud to introduce to you an updated **EmulationStation** frontend!


This new version introduces several theming improvements:

* The System Carousel is now fully themeable! You can finally theme or remove the white boxes behind logos and...


[![new carousel](/images/blog/2018-03-16-emulationstation-theming/new-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-carousel.png)


* You can have a **vertical Carousel** and place it wherever you want!!!


[![standard](/images/blog/2018-03-16-emulationstation-theming/standard_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/standard.png)

[![vertical](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel.png)



* The HelpSystem (the small icons and information at the bottom of the screen) is also themable now!
* You can also change the background, colors, fonts and more in the menus!


[![menus](/images/blog/2018-03-16-emulationstation-theming/menus_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/menus.png)

[![new menu](/images/blog/2018-03-16-emulationstation-theming/new-menu_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-menu.png)



We also introduced a new system of *subsets* in themes and a *$system* variable:

You can have several *variations* of different theme components and choose them directly in **EmulationStation**


[![theme config](/images/blog/2018-03-16-emulationstation-theming/theme-config_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config.png)

[![subset config](/images/blog/2018-03-16-emulationstation-theming/theme-config2_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config2.png)



* colorset: add as much colors variations as you want to your theme
* iconset: choose between different sets of icons for the helpsystem
* menu: make different menu themes selectable from *ES*
* systemview: select your prefered system view with different carousel modes
* gamelist: same as above
* region: you can add a tag on theme elements (EU/US/JP) and select the ones you want to display from here!

The system variable allows you to make one "theme.xml" file for all systems!

To show you all the possibilities that came with this update, we updated the recalbox theme with lots of options:

* iconsets for SNES/PSX/XBox controllers
* new menus
* lots of systemviews including adapted ones for CRT/Small and vertical screens



[![CRT](/images/blog/2018-03-16-emulationstation-theming/crt-jp_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/crt-jp.png)



* differents gamelistviews including big picture mode

You will find more information about **EmulationStation** theming [*here*](https://gitlab.com/recalbox/recalbox-emulationstation/blob/master/THEMES.md) and you can check how the new recalbox theme is made.