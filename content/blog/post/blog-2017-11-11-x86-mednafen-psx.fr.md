+++
date = "2017-11-11T14:00:00+00:00"
image = "/images/blog/2017-11-11-mednafen-psx/recalbox-mednafen-psx-banner.png"
title = "Nouveaux cores PSX pour X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hello les potos,

Le core PSX actuellement présent dans Recalbox fait un travail remarquable avec les boards ARM.
Mais il ne peut pas gérer des options de rendu avancées sur les versions X86.

Donc pour pallier à ce problème, nous avons ajouté les [cores libretro mednafen psx](https://github.com/libretro/beetle-psx-libretro).

Le core **mednafen_psx_hw** est une version OpenGL améliorée, avec un moteur de rendu OpenGL.
Il nécessite un GPU au moins compatible OpenGL 3.1.
Donc si votre matériel n'est pas compatible, vous pourrez toujours utiliser le core **mednafen_psx**.

Voici quelques options disponibles avec ces cores :

- OpenGL renderer
- Internal GPU Resolution (1x(native)/2x/4x/8x/16x/32x)
- Texture filtering (nearest/SABR/xBR/bilinear/3-point/JINC2)
- Internal color depth (dithered 16bpp (native)/32bpp)
- PGXP perspective correct texturing
- Widescreen mode hack
- CPU Overclock
- CHD games support

Plus d'informations disponibles dans la [documentation de libretro](https://buildbot.libretro.com/docs/library/beetle_psx_hw/).

Ces nouveaux cores nécessitent [l'ajout de nouveaux bios](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) dans votre Recalbox :

- **8dd7d5296a650fac7319bce665a6a53c** - **scph5500.bin**
- **490f666e1afb15b7362b406ed1cea246** - **scph5501.bin**
- **32736f17079d0b2b7024407c39bd3050** - **scph5502.bin**

Ces cores sont assez difficiles concernant les bios. Merci donc de bien respecter les noms et signatures de fichiers.

Nous espérons que vous allez apprécier ces nouveaux cores et que vous allez (re)découvrir la fantastique ludothèque de la PSX tout en profitant de graphismes modernes.

## Résolution GPU interne

[![Internal GPU Resolution](/images/blog/2017-11-11-mednafen-psx/resolution-thumb.png)](/images/blog/2017-11-11-mednafen-psx/resolution.png)

## Filtrage des textures

[![Textures filtering](/images/blog/2017-11-11-mednafen-psx/textures-thumb.png)](/images/blog/2017-11-11-mednafen-psx/textures.png)

## PGXP - correction des perspectives

[![PGXP perspective correct texturing](/images/blog/2017-11-11-mednafen-psx/perspective-thumb.png)](/images/blog/2017-11-11-mednafen-psx/perspective.png)
