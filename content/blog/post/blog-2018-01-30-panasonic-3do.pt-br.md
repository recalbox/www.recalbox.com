+++
date = "2018-01-30T11:00:00+02:00"
image = "/images/blog/2018-01-30-panasonic-3do/recalbox-panasonic-3do-banner.png"
title = "Panasonic 3DO"

[author]
  github = "https://github.com/OyyoDams"
  gitlab = "https://gitlab.com/OyyoDams"
  name = "OyyoDams"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
+++

Olá amigos!

Estamos felizes em presentear vocês com um console pouco reconhecido e mesmo assim com uma performance incrível durante o seu lançamento: o **Panasonic 3D0!**

Infelizmente, este novo sistema só estará disponível para as versões do Recalbox Odroid XU4 e PC X86 / X86_64. E no Raspberry PI, até mesmo na versão 3, não atingimos uma emulação satisfatória mesmo utilizando as configurações mais baixas + frameskip ativo. Por conta disto, decidimos não habilitar este sistema em outras versões do Recalbox.

Para emular este sistema, utilizaremos o [core libretro 4do](https://github.com/libretro/4do-libretro) baseado no emulador [4DO](http://www.freedo.org/).

Para sua informação, este core precisa de uma nova bios. Este é o arquivo a ser [adicionado ao seu Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) : 

- **51f2f43ae2f3508a14d9f56597e2d3ce** - **panafz10.bin**

Esperamos que vocês curtam este novo core e (re)descubram a biblioteca de jogos do Panasonic 3D0.

## Um vídeo review dos 30 melhores jogos do Panasonic 3D0

<iframe width="560" height="315" src="https://www.youtube.com/embed/un6u1nmvyTo" frameborder="0" allowfullscreen></iframe>

