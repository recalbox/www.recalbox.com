+++
date = "2017-03-06T16:43:03+02:00"
image = "/images/blog/2017-03-06-manager2/Webmanager2.jpg"
title = "Saiu o Recalbox Web Manager 2.0 !"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

[translator]
   github = "https://github.com/nwildner"
   gitlab = "https://gitlab.com/nwildner"
   name = "nwildner"


+++
<p dir="auto">Olá, hoje apresentamos o magnífico trabalho feito por um membro da comunidade.</p><p dir="auto">Não sei se todo mundo sabe, mas a versão atual do recalbox já possui uma<strong>interface web</strong> integrada, que permite que você gerencie as configurações do seu recalbox.</p><p dir="auto">Porém, alguns meses atrás, <strong><a href="https://github.com/DjLeChuck" target="_blank">DjLeChuck</a></strong>, demonstrou que o gerenciador atual não estava completo nem era suficiente e decidiu desenvolver um novo.</p><p dir="auto">Esta nova interface web te dá mais opções, habilidades para <strong>gerenciar informações dos seus jogos</strong>, seus <strong>screenshots</strong>, <strong>atividades do EmulationStation</strong> e mais !<br>DjLeChuck fez um ótimo trabalho, e estamos orgulhosos de integrá-lo ao lançamento da versão 4.1.0.</p><p dir="auto">Se você deseja ajudar no desenvolvimento desta ferramenta, você pode reportar incidentes no <a href="https://github.com/DjLeChuck/recalbox-manager" target="_blank">repositório do github</a> ou ajudar na <a href="https://poeditor.com/projects/view?id=93183" target="_blank">tradução</a>.</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-06-manager2/es_manager_1.jpg" target="_blank"><img src="/images/blog/2017-03-06-manager2/es_manager_1.jpg" alt="es_manager_1"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-06-manager2/es_manager_2.jpg" target="_blank"><img src="/images/blog/2017-03-06-manager2/es_manager_2.jpg" alt="es_manager_2"></a></div><p>Você pode optar por utilizar a primeira versão do gerenciador alterando as configurações no arquivo recalbox.conf :<br></p><p style="color:gray">## Recalbox Manager (http manager)<br>system.manager.enabled=1<br>## 1 or 2, depending on the manager version you wish<br>system.manager.version=1<br></p><p dir="auto">Aqui, um pequeno vídeo de demonstração :</p><iframe width="560" height="315" src="https://www.youtube.com/embed/l1sxi1w1mVY" frameborder="0" allowfullscreen=""></iframe>
