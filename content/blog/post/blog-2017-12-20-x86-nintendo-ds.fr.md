+++
date = "2017-12-20T10:00:00+00:00"
image = "/images/blog/2017-12-20-nintendo-ds/recalbox-nintendo-ds-banner.png"
title = "Nintendo DS pour X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hello les amis,

Il est temps d'accueillir un nouveau système émulé dans la version **X86/X86_64** de Recalbox : **La Nintendo DS !**   

Pour réaliser cette tache, nous avons donc ajouté 2 émulateurs: le core [libretro desmume](https://github.com/libretro/desmume) et le core [libretro melonDS](https://github.com/libretro/melonDS) qui est basé sur un nouvel émulateur Nintendo DS.   
Pour émuler convenablement ce nouveau système, vous **devez** avoir un bon PC. Nous avons configuré le core desmume afin qu'il puisse fonctionner sur la plupart des configurations actuelles. Mais si n'obtenez pas une émulation exécutée à pleine vitesse, vous pouvez essayer de baisser les réglages dans le menu de retroarch (baisser la résolution interne, ajouter un frameskip, etc...). A l'opposé, si vous possédez une PC véloce, vous pouvez utiliser ce même menu pour booster les réglages et ainsi améliorer le rendu graphique.   

Malheureusement, ce nouveau système ne sera disponible que pour les versions X86/X86_64 de Recalbox. En effet, même avec la plus puissante des cartes ARM compatible Recalbox, l'Odroid XU4, nous n'obtenons pas une émulation satisfaisante et cela même en utilisant les réglages les plus bas + l'activation d'un frameskip.   
Nous avons donc décidé, dans l'état actuel des choses, de ne pas activer ce système dans les autres versions de Recalbox.

Pour votre information, le core **libretro melonDS** nécessite l'ajout de nouveaux bios. Voici les fichiers [à ajouter dans votre Recalbox](https://github.com/recalbox/recalbox-os/wiki/Ajoutez-des-bios-%28FR%29) :

**df692a80a5b1bc90728bc3dfc76cd948** - **bios7.bin**   
**a392174eb3e572fed6447e956bde4b25** - **bios9.bin**   
**e45033d9b0fa6b0de071292bba7c9d13** - **firmware.bin**   

Nous espérons que vous allez apprécier ces nouveaux cores et que vous allez (re)découvrir la fantastique ludothèque de la Nintendo DS.

## Différents modes d'affichage (standard/hybride)

Standard:   
[![display_standard](/images/blog/2017-12-20-nintendo-ds/display-standard-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-standard.png)

Hybride:   
[![display_hybrid](/images/blog/2017-12-20-nintendo-ds/display-hybrid-thumb.png)](/images/blog/2017-12-20-nintendo-ds/display-hybrid.png)

## Shader Retro

[![shader_retro_mario](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-mario.png)
[![shader_retro_kart](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-retro-kart.png)

## Shader Scanlines

[![shader_scanlines_mario](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-mario.png)
[![shader_scanlines_kart](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart-thumb.png)](/images/blog/2017-12-20-nintendo-ds/shader-scanlines-kart.png)
