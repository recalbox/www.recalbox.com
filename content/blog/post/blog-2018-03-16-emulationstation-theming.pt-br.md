+++
date = "2018-03-16"
image = "/images/blog/2018-03-16-emulationstation-theming/18.03.16_release-stable-visual.jpg"
title = "EmulationStation Theming"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++


Olá a todos!

Agora que temos um sistema atualizado e estável, vamos trazer algumas belas melhorias cosméticas!

O time Recalbox tem orgulho de introduzir a vocês o novo e atualizado frontend **Emulationstation**!

Esta nova versão introduz diversas melhorias nos temas:

* O Carrossel do Sistema está altamente customizável! Você pode finalmente alterar ou remover as caixas brancas atrás dos logos e...


[![new carousel](/images/blog/2018-03-16-emulationstation-theming/new-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-carousel.png)


* Você pode ter um **Carrosel vertical** posicionado onde quiser!!!


[![standard](/images/blog/2018-03-16-emulationstation-theming/standard_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/standard.png)

[![vertical](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/vertical-carousel.png)



* O HelpSystem (pequenos ícones e informações no rodapé da tela) também possuem suporte a temas!
* Você tambem pode alterar o plano de fundo, cores, fontes e muito mais nos menus!


[![menus](/images/blog/2018-03-16-emulationstation-theming/menus_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/menus.png)

[![new menu](/images/blog/2018-03-16-emulationstation-theming/new-menu_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/new-menu.png)



Nós também introduzimos um novo sistema de *subsets* nos temas e a variável *$system*:

Você pode ter diversas *variações* de diferentes componentes dos temas que podem ser alterados diretamente no **EmulationStation**


[![theme config](/images/blog/2018-03-16-emulationstation-theming/theme-config_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config.png)

[![subset config](/images/blog/2018-03-16-emulationstation-theming/theme-config2_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/theme-config2.png)



* colorset: adiciona quantas variações de cor você desejar em seu tema
* iconset: escolha entre diversos grupos de ícones para o helpsystem
* menu: torna diferentes temas de menu selecionáveis do *ES*
* systemview: seleciona sua visualização de sistema preferida com diferentes tipos de carrossel
* gamelist: o mesmo do item acima
* region: você pode adicionar uma tag em elementos do tema (EU/US/JP) e selecionar quais deseja exibir!

A variável system permite que você crie um "theme.xml" para todos os sistemas!

Para mostrar as possibilidades do que podemos alcançar com esta atualização, incrementamos o tema do recalbox com diversas opções:

* iconsets para controles de SNES/PSX/XBox
* novos menus
* diversas systemviews incluindo adaptadas para monitores pequenos, CRT e telas verticais



[![CRT](/images/blog/2018-03-16-emulationstation-theming/crt-jp_tn.jpg)](/images/blog/2018-03-16-emulationstation-theming/crt-jp.png)



* diferentes gamelistviews incluindo o modo big picture

Você encontrará maiores informações sobre temas do **EmulationStation** [*aqui*](https://gitlab.com/recalbox/recalbox-emulationstation/blob/master/THEMES.md) e poderá verificar como o novo tema do recalbox é construído.
