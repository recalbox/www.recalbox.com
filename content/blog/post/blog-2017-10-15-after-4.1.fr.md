+++
date = "2017-10-15T16:46:06+02:00"
image = "/images/blog/2017-10-15-after-4.1/gitlab.jpg"
title = "La 4.1... et après ?"

[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

[translator]
  github = "https://github.com/lioda"
  gitlab = "https://gitlab.com/lioda"
  name = "lioda"
+++

Recalbox 4.1 est disponible pour tous depuis le vendredi 13 octobre à 20h.

Au moment où j'écris cet article, ce sont plus de **17 000** images pour de nouvelles installations qui ont été téléchargées et **plus de 10 000** recalbox qui ont été mises à jour.

![](/images/blog/2017-10-15-after-4.1/download-4.1.png)

Merci à vous pour vivre cette aventure avec nous, vous êtes la meilleure communauté dont nous puissions rêver !

#### Déménager de Github à Gitlab
Recalbox 4.1 a été l'occasion de réaliser de nombreux changements, et pas seulement au niveau des logiciels, mais aussi au sein de nos outils.

Nous avons décidé de déplacer nos projets de Github vers Gitlab parce que nous trouvons cette plateforme mieux intégrée et plus pratique. Nous avions besoin de plusieurs de ses fonctionnalités comme, par exemple, gitlab-ci qui est maintenant au centre de notre chaîne de build automatique.

Maintenant, vous pouvez accéder aux sources ici : https://gitlab.com/recalbox/recalbox

Et tous les autres projets sont disponibles dans le group recalbox : https://gitlab.com/recalbox


#### Revues
Vous aimez le danger ? Vous vous considérez comme un *early adopter* ?

Nous avons besoin de testeurs !

Recalbox nécessite les talents d'une véritable équipe pour évaluer nos fonctionnalités expérimentales. C'est un grand pouvoir... et une grande responsabilité. Tout le monde peut participer en nous faisant des retours.

Avec notre tout nouvel outil de déploiement, le-a testeur-euse n'aura qu'à changer une ligne dans le fichier *recalbox.conf* et mettre à jour sa recalbox de façon standard.

#### Des versions plus souvent
Vous comme nous sommes fatigués d'attendre un an entre chaque sortie de version.

Nous avons revu tout le processus et les outils pour déployer les nouveautés une par une, dans un flux d'évolution continu. La route a été longue pour rendre cela possible sans dégrader votre expérience en tant qu'utilisateur de Recalbox. Mais maintenant, tout est prêt pour renforcer votre plaisir.

#### Canari
Dans le cas (très improbable) d'un problème lors d'une mise à jour, nous ne voudrions pas altérer toutes les recalbox du monde.

Nous avons donc mis en place un système de *Canary Release*. Aussitôt qu'une nouvelle version est prête, seul un petit pourcentage de recalbox sera impacté, le temps pour nous de s'assurer que tout se déroule sans problème. Ce n'est qu'après, que nous libérerons la bête :)


#### Et ensuite ?
Puisque votre recalbox sera actualisée le plus souvent possible, nous travaillons sur un nouveau mécanisme de téléchargement et de démarrage de RecalboxOS. De cette façon, nous espérons réduire le temps de mise à jour de 50% à 80%.

Parfaitement : 80% ! En fait, le processus ne consistera plus qu'en une étape : télécharger le fichier de mise à jour. Finies les copies sur partition racine lors du redémarrage et toutes ces opérations fastidieuses.

Et ce n'est que le début. Recalbox 4.1 est une étape majeure dans le projet. Continuez de nous suivre... si vous ne passez pas tout votre temps à jouer à tous ces merveilleux grand classiques de l'âge d'or du jeu vidéo !
