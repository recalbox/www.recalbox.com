+++
date = "2018-04-20T01:00:00+02:00"
image = "/images/blog/2018-04-20-recalbox-18-04-20/recalbox-18.04.20-banner.png"
title = "Recalbox 18.04.20"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  name = "abunille"
+++


Hallo zusammen!

Wir haben in letzter Zeit weiter an Recalbox gearbeitet, um einige neue Funktionen zu integrieren. Erneut sind vielen Änderungen an EmulationStation hinzugekommen. Ihr werdet feststellen, dass wir an einer besseren Internationalisierung von Recalbox gearbeitet haben, indem wir die Übersetzungen vieler Freiwilliger aktualisiert haben. Wenn ihr auch helfen wollt, klickt [hier für emulationStation](https://poeditor.com/join/project/hEp5Khj4Ck), oder [hier für den Webmanager](https://poeditor.com/join/project/taFNFlZ840).

Wir haben auch viel Zeit investiert, um eine bessere Soundkonfiguration verwenden zu können. Das bedeutet dynamische Lautstärkeänderung, oder noch besser: dynamische Soundkartenausgabe! Ja, ihr könnt die Soundkarte jetzt on-the-fly ändern! Der Lautstärke-Fehler beim Update ist noch nicht behoben, aber wir sind guter Dinge für das nächste Release!

Also, hier nun der detaillierte Changelog:

* **ES: Pop-ups hinzugefügt** Zur Zeit wird nur der Name der abgespielten Musikdatei angezeigt, aber die Grundlagen stehen. Dies wird noch verbessert und vor allem für die Zukunft genutzt.

* **ES: Tag im Theme hinzugefügt, um den Übergangsstil zu erzwingen** Einige Themes (wie Fuzion) sehen mit einem bestimmten Übergang besser aus. Es gibt jetzt einen Tag (Tag: hier meinen wir das englische Wort), um den Übergangsstil in den Themes zu erzwingen.

* **kodi-plugin-video-youtube auf 5.4.6 aktualisiert** Nur, um es aktuell zu halten. Die vorherige Version ist nicht mehr verfügbar und kann einige Kompilierungsfehler verursachen.

* **Advancemame auf 3.7 aktualisiert und Spinner-Unterstützung hinzugefügt** Ein kleines Update für AdvanceMAME, das nun Mausunterstützung bietet. Das ist eine gute Sache für Spinner (Spinner: auch hier meinen wir das englische Wort) und Lightguns.

* **ES: Dynamischer Soundkartenwechsel** Dies hier wird euch gefallen. Ihr könnt jetzt die Soundkarten in ES dynamisch ändern. Ich denke, x86-Benutzer werden dieses Update genießen ;)

* **fix: Standard-Schriftart für ES und RA verschoben** Erinnert ihr euch an den gelben Quadrate Bug? Dieser Fix verhindert, dass er zurückkommt. Die Schriftarten wurden verschoben.

* **ES: Dynamische Änderung der Lautstärke und der Audioausgabe im Menü** Selbsterklärend, oder? Ihr müsst das Fenster nicht verlassen, um die neue Lautstärke zu hören. Es wird dynamisch aktualisiert, wenn ihr den Schieberegler bewegt!

* **ES: ubuntu_condensed.ttf als Fallback-Font hinzugefügt** Dies sollte bei nicht-lateinischen Alphabeten helfen. Wir müssen noch immer eine Schriftart finden, die mit Koreanisch umgehen kann.

* **ES: Uhr im Hauptmenü hinzugefügt** Drückt Start, um die Uhrzeit zu sehen! Falls die Uhr nicht angezeigt wird, aktiviert diese Option in den UI Einstellungen.

* **ES: Fehlende Favoriten-Icons für Amiga, 3DO und X68k hinzugefügt** Kann man nicht besser ausdrücken...

* **ES: Das Nachladen des Themes wurde korrigiert, wenn gamelistonly=0** Das Theme sollte jetzt schneller geladen werden.

* **ES: Hilfemeldungen übersetzt** Ja, endlich...

* **ES: Hilfemeldung "QUIT" im Hauptmenü hinzufügt** Die SELECT-Taste hatte bisher keinen Hinweis auf ihre Rolle in der Systemansicht.

* **PPSSPP-Version auf die Vorgängerversion zurückgesetzt** Viele Nutzer berichteten von großen Problemen mit der PPSSPP Version 1.5.4. Aus diesem Grund haben wir sie auf die Vorgängerversion zurückgesetzt.

* **Mehr Informationen im Support-Archiv** Das ist für uns, um euch besser helfen zu können!

* **Upgrade: Korrektur des Upgrades von einem benutzerdefinierten Zweig auf Stable** Ohne einige manuelle Änderungen konnten wir die Testzweige nicht einfach aktualisieren. Jetzt können wir es!

* **Recallog protokolliert jetzt auch dann, wenn die Freigabe nicht gemountet ist** Es ist so viel besser zu wissen, was beim Booten passiert, auch wenn die Recalbox-Protokolle nicht bereit sind. Das ist etwas, was wir in den nächsten Aktualisierungen noch weiter verbessern werden.

* **Fix: Wifi immer beim Neustart aktiviert, auch wenn in ES deaktiviert** Wifi wurde beim Bootvorgang immer gestartet, auch wenn es deaktiviert war. Ja, da hatten wir etwas übersehen...

* **Aufteilung Share-Erkennung und Update-Prozess in 2 Scripte** Das war ein Boot-Skript, das zu viel Arbeit erledigte. Die 2 Hauptprozesse sind jetzt in 2 Skripte aufgeteilt, das ist für uns einfacher zu pflegen.

* **mame2010: highscores hinzugefügt** Nun mit Highscores! Aber der Ratio-Bug ist noch vorhanden.

* **fix: fmsx konnte nicht als MSX-Kern ausgewählt werden** Jetzt ist er verfügbar!

* **DosBox: Lag-Problem auf RPI3 gelöst + seltsames Verhalten, wenn keine dosbox.bat vorhanden war** Es funktioniert jetzt wieder.

* **SDL2: Fehler auf x86 behoben, der ScummVM auf dem Startbildschirm blockiert** Jetzt könnt ihr die Spiele ohne Probleme starten!

* **Odroid XU4: verbesserte HDMI-Stabilität, boot.ini-Optionen** Personen, die den XU4 auf einem HDMI-CEC-Fernseher verwenden, konnten mit verschiedenen Problemen konfrontiert werden. Sollte jetzt stabil sein.

* **fix: Game&Watch Kernname richtig angezeigt** Kann ich nicht besser ausdrücken!

* **Manager: Übersetzungen aktualisiert** Der Webmanager hat einige aktualisierte Übersetzungen spendiert bekommen!

Ziemlich gut, oder?
