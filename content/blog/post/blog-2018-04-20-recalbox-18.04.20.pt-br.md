+++
date = "2018-04-20T01:00:00+02:00"
image = "/images/blog/2018-04-20-recalbox-18-04-20/recalbox-18.04.20-banner.png"
title = "Recalbox 18.04.20"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Olá a todos!


Estamos trabalhando continuamente no Recalbox para trazer novas funcionalidades, e novamente,  diversas alterações no EmulationStation. Você notará que estamos trabalhando em uma melhor internacionalização do Recalbox através de atualizações feitas por diversos voluntários. Se você deseja ajudar [acesse aqui para colaborar com o EmulationStation](https://poeditor.com/join/project/hEp5Khj4Ck) ou [aqui para o webmanager](https://poeditor.com/join/project/taFNFlZ840).

Também investimos um bom tempo para melhorar a configuração do som. Isto envolve mudança dinâmica de volume e mais: saída de som dinâmica! Isso mesmo, você pode alterar seu dispositivo de áudio em tempo de execução! O bug do volume na atualização ainda não foi resolvido, mas temos esperanças para o próximo lançamento!

Então, aí vai a lista de mudanças detalhadas:

* **ES: Adicionadas pop-ups** Por enquanto serve apenas para exibir o nome da música sendo tocada, mas a base está aí, e iremos melhorar esta funcionalidade e utilizá-la mais e mais.

* **ES: Adicionada tag no tema para forçar estilo de transição** Alguns temas (como o fuzion) tem melhor aparência com transições específicas. Agora existe uma tag para forçar o estilo de transição nos temas.

* **Atualizado kodi-plugin-video-youtube to 5.4.6** Atualizado, já que a versão anterior não estava mais disponível e causava alguns erros de compilação

* **Atualizado Advancemame para 3.7 e suporte a spinner adicionado** Uma pequena atualização para o AdvanceMAME, que habilita o suporte ao mouse. Isto é algo interessante para spinners, e possivelmente para lightguns.

* **ES: Corrigida a seleção de placa de áudio** Esta você irá adorar ! É a alteração dinâmica de saída de audio para o ES. Acho que usuários x86 irão aproveitar bastante esta funcionalidade ;)

* **correção: fonte padrão para ES e RA incorporadas ao sistema** Lembram do bug dos quadrados amarelos ? Esta correção foi feita para previnir este fato já que algumas fontes mudaram de localização

* **ES: Adicionada mudança dinâmica de volume e saída de áudio no menu** Autoexplicativo né ? Não é mais necessário sair da janela para ouvir o novo volume do som, sendo dinâmico durante a movimentação do indicador !

* **ES: ubuntu_condensed.ttf adicionada como fonte alternativa** Isto irá ajudar com alfabetos não latinos. Ainda precisamos encontrar uma fonte com suporte a Língua Coreana.

* **ES: Adicionado relógio ao menu principal** Aperte start para ver que horas são !

* **ES: Adicionados fav icons ausentes para Amigas, 3DO e X68k** Não preciso explicar essa...

* **ES: Corrigido quanto tema é recarregado com a opção gamelistonly=0** o tema deverá carregar um pouco mais rápido

* **ES: Corrigido HelpMessages not translated** Bom ... Parece pegadinha do 1º de Abril, mas a tradução ainda não funciona nesta parte :/

* **ES: Adicionada mensagem de ajuda de "SAIR" no menu principal** O botão SELECT não possuia dicas para a system view até agora

* ** PPSSPP revertido para a versão anterior** devido a grande demanda gerada por problemas na versão 1.5.4, a atualização foi revertida para a versão anterior

* **mais informações nos arquivos de suporte** Para que nós, possamos ajudá-lo !

* **atualização: corrigida atualização de branch custom para estável** Não conseguíamos atualizar facilmente versões de teste sem algumas edições manuais. Agora conseguimos!

* **recallog guarda informação mesmo com compartilhamento não montado** Muito melhor saber o que se passa durante a inicialização, mesmo quando os logs do Recalbox ainda não estão prontos. Isto é algo que melhoraremos ainda mais para as próximas versões

* **correção: wifi permanecia ativa após reboots mesmo estando desabilitada no ES** Wifi estava sempre ligada durante a inicialização mesmo se estivesse desabilitada. Corrigido...

* **detecção de compartilhamento e processo de atualização separados** Era um script na inicialização fazendo todo o serviço. Os 2 processos foram separados em 2 scripts, tornando mais fácil a manutenção para nós

* **mame2010: habilitado highscore** O Que?! Mas proporções ainda precisam ser corrigidas.

* **correção: fmsx não podia ser selecionado como núcleo para o MSX** Agora pode!

* **DosBox: regressão que causava travamentos no RPI3 + comportamento estranho no dosbox.bat** Agora tudo funciona como o esperado!

* **SDL2: corrigido bug do x86 que travava o ScummVM no splash screen** Agora você consegue jogar!

* **Odroid XU4: melhorada a estabilidade da HDMI, opções boot.ini** Quem usasse a XU4 em uma TV com HDMI CEC enfrentaria vários problemas. Agora está estável

* **correção: núcleo Game&Watch não exibia corretamente a imagem** Também resolvido!

* **manager: traduções atualizadas** O web manager teve as traduções atualizadas!

Bem bacana, não acham ?
