+++
date = "2019-03-22T11:54:12Z"
image = "/images/blog/2019-03-22-upcoming-stable-release/banner.jpg"
title = "Version stable en approche"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"

+++

Bonne nouvelle !

Nous sommes à quelques jours de sortir la version stable de Recalbox !

Mais avant, on a besoin de votre aide !
Aidez-nous à détecter tout éventuel bug qui aurait pu nous échapper, avant la sortie finale.

## Noms de versions

Comme vous l'avez probablement déjà remarqué, il y a quelques temps on a changé la manière dont on nommait les versions de Recalbox: on utilisait précédemment des numéros (`4.0`, `4.1`, …) puis, au lieu de passer sur la version `5.0`, on est passé à des versions basées sur la date (`18.04.20`, `18.06.27`, …).

Malheureusement, les dates sont difficiles à lire, difficiles à écrire et difficiles à prononcer ou à retenir. En un mot : c'est pas pratique.

Du coup, on a décidé de **revenir aux numéros** qui sont plus faciles à utiliser, plus explicites (on connait l'importance d'une version selon son numéro, alors qu'une date de donne en fait que peu d'information sur le contenu).

On considère donc que toutes les version "datées" font parties de la branche `5.x` et on va donc revenir à une version… `6.0` 🎉

Comme pour nos enfants, on va probablement donner un nom aux versions qu'on aime le plus 😉
Rien n'a encore été décidé pour la stable qui arrive, mais elle va clairement recevoir un nom ! ❤️

## Release Candidate 1

Notre équipe de développement a passé les derniers mois à accumuler plein d'optimisations, de nouvelles fonctionnalités, mises-à-jours, thèmes, systèmes, émulateurs, matériels supportés, … On ne va pas entrer dans les détails croustillants ici, mais attendez-vous à une description précise dans le blog post qui annoncera la sortie finale.

On a tellement apporté de modifications au code, qu'on a décidé de tester nos optimisations en long en large et en travers ces derniers mois pour éviter tout soucis. C'est en grande partie pour ça qu'on a tardé à sortir cette version. On a effectué autant de tests que possible pour que votre expérience de jeu soit la plus agréable possible.

Et tout ça, c'est (aussi) grace à vous !

Avec l'aide de notre super communauté (assistée de notre tout-aussi-super équipe de support!), on a pu sortir au moins 3 différentes versions _beta_ qui ont soulevé des dizaines de points, que nos développeurs ont pris le temps de corriger aussi vite que possible (vous vous souvenez que tout le monde est bénévole, dans notre équipe ?).

On en aura parcouru du chemin pour vous présenter cette nouvelle version de Recalbox !

Mais aujourd'hui, on est super confiants et super fiers de notre travail, à tel point que cette version, on pourrait la sortir telle-qu'elle d'ici quelques jours.

Mais ce n'est pas suffisant! (encore) On veut que cette prochaine version stable soit vraiment ce qu'on attend d'elle : stable 💪

C'est pourquoi **on publie aujourd'hui ce qu'on appelle une Release Candidate (RC)**. Comme son nom l'indique bien, c'est une version qui est une bonne candidate pour être la version finale. On la publie quelques jours avant la sortie finale pour que des gens puissent la tester sur un éventail encore plus large de matériel, de configurations, de _bios_, de _roms_ pour détecter des bugs critiques qui nous auraient échappés 😬

Comme vous le savez peut-être, les versions _beta_ s'adressent à des utilisateurs qui savent trouver, reproduire et correctement décrire un bug (en fournissant des extraits de fichiers de log). Une Release Candidate, a contrario, est utilisable par n'importe quel utilisateur moyen susceptible de nous décrire comment reproduire un bug (et c'est déjà pas mal !). Elle devrait être évitée par les gens qui ne savent pas encore comment flasher une carte SD, tester quelques jeux ou configurer une manette, en revanche.

**Ce qu'on attend de vous** si vous voulez tester, c'est de [télécharger l'image](https://recalbox-releases.s3.nl-ams.scw.cloud/stable/index.html) disponible pour votre matériel, la configurer à votre goût (options, jeux, thèmes, …) ou même utiliser votre configuration d'une version précédente et nous prévenir si quelque chose a l'air cassé 💥

Si vous rencontrez un problème critique, contactez-nous sur [Discord](https://discord.gg/d2xCQ4e) en pensant bien à nous donner un maximum d'informations afin qu'on puisse comprendre le soucis et vous aider le plus efficacement possible:

* version de Recalbox (il y aura peut-être plus d'une RC)
* matériel utilisé: carte, alimentation et manettes
* le thème que vous utilisez
* les éventuelles modifications de configuration dans les options
* les étapes détaillées pour reproduire le bug
* le comportement attendu
* le comportement constaté

Certaines choses sont hors du champs de cette RC et ne devraient pas être rapportées particulièrement (elles doivent être rapportées mais ne seront fixées qu'après la sortie finale):

* soucis liés à une manette spécifique
* soucis liés à une _rom_ spécifique
* soucis liés à un thème spécifique
* bugs/soucis déjà connus (qui seront corrigés après la sortie):
  * les _cheats_ ne fonctionnent pas sur Mame2010
  * format d'écran sous Retroarch différent de sous ES/Configgen
  * Kodi ne démarre pas avec certaines manettes

## Mises-à jour (non !)

Si vous utilisez déjà la dernière version stable de Recalbox, ou une de nos dernières versions _beta_ et que vous voulez mettre à jour vers cette RC par le système de mise-à-jour intégré: **ne le faites pas !**

On a tellement changé le code de Recalbox que le système de mise-à-jour et l'infrastructure sous-jacente ne sont plus compatibles et il n'est malheureusement pas possible de mettre à jour depuis une version précédente.

Pour installer cette nouvelle version : repartez sur une installation propre, comme expliqué dans ce tuto:

<iframe width="560" height="315" src="https://www.youtube.com/embed/1UBH4S6kG80" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Testez, jouez, bidouillez, amusez-vous...** et dites-nous comment ça s'est passé ! 🤗
