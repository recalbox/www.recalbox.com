+++
date = "2017-02-23T16:27:35+02:00"
image = "/images/blog/2017-02-23-help/title-help.jpg"
title = "New frontend Help Popups!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++
<p dir="auto">Aujourd'hui laissez nous vous présenter une nouvelle fonctionnalité de la version 4.1</p><p dir="auto">Lorsque vous découvrez et configurez votre OS de rétro-gaming préféré, il n'est pas toujours possible d'aller lire la documentation en ligne pour avoir des informations sur chaque option.</p><p dir="auto">Pour pallier à cela, nous avons donc décidé d'ajouter la possibilité d'afficher à l'écran <strong>des fenêtres d'aide</strong>, en pressant la touche <strong>Y</strong> de votre manette.</p><p dir="auto">Une fenêtre va alors apparaître ainsi : </p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets//images/blog/2017-02-23-help/es_help.jpg" target="_blank"><img src="/images/blog/2017-02-23-help/es_help.jpg" alt="es_help"></a></div><p dir="auto">Voici une courte vidéo de démonstration :</p> <iframe width="560" height="315" src="https://www.youtube.com/embed/Tv3c_zC2Mfg" frameborder="0" allowfullscreen=""></iframe>