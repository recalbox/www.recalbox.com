+++
date = "2018-06-27"
image = "/images/blog/2018-06-27-netplay/recalbox-18.06.27-banner.jpg"
title = "Netplay"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"

+++


Here comes a new challenger!!!

Ever dreamt about playing your retro games online, unlock achievements like you do with your next gen console?

Well... Retroarch made it and Recalbox now brings it to you with the usual user-friendly style!


To get started, go to the menu / Games Settings / Netplay Settings


[{{< img src="/images/blog/2018-06-27-netplay/netplay-settings.png" alt="netplay settings">}}](/images/blog/2018-06-27-netplay/netplay-settings.png)



From here, you can activate/deactivate Netplay, set your Netplay username, choose a port.

You can also add CRC32 hash to your gamelist for a better match in Netplay lobby:



[{{< img src="/images/blog/2018-06-27-netplay/hash.png" alt="hash">}}](/images/blog/2018-06-27-netplay/hash.png)



Please note that it can take quiet some time for large romsets (less than scrapping) but it is highly recommended for better compatibility.
You can only add hash to existing gamelists (it means you games must be scrapped).



Now you are ready to go!!!!

In the gamelist of authorized systems (fba_libretro,mame,mastersystem,megadrive,neogeo,nes,pcengine,sega32x,sg1000,supergrafx) you can now launch any game as *host* with X button instead of B

*Please note that there may be perfromance issues with some of these cores*


[{{< img src="/images/blog/2018-06-27-netplay/x.png" alt="x">}}](/images/blog/2018-06-27-netplay/x.png)



Ok, we can launch a game as host but how to join an existing game you would say? easy!!!

From the main screen (systemview), simply hit X and it will bring you a new GUI listing all available Netplay games!



[{{< img src="/images/blog/2018-06-27-netplay/lobby.png" alt="lobby">}}](/images/blog/2018-06-27-netplay/lobby.png)


For each game you will have a lot of information:

* Username (with a small icon if it's a game launched from Recalbox)
* Country
* Hash match (if you have the same rom with the same hash in your games)
* File match
* Core
* Connection latency and other information

You will see a *result* at the bottom (and in front of the game name in the list) telling you how likely you will be able to join:

* Green: you have the good rom with good hash, core is ok, there is high chance it will work
* Blue: hash not match (but some roms don't have hash e.g. arcade roms) but file is found on your system and core is ok. it *should* work
* Red: No file found, unauthorized system, no core match: no chance it will work (game won't launch)

Ready Player One?