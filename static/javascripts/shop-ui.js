function populateCategory(categoryObj,index) {
            var category = $(".recal_category_" + index)
            $(".recal_category_title", category).text(categoryObj.name)
            // Item list
            var i = 1;
            for (var itemIndex in categoryObj.items) {
                var currentrow;
                if (i % 3 == 1) {
                    currentrow = $("<div class='row'></div>")
                    $(".recalbox_main_list", category).append(currentrow)
                }
                var item = categoryObj.items[itemIndex];
                var newItem = $(".recal_product_template").clone()
                $(".recal_prod_title", newItem).text(item.title)
                $(".recal_prod_link", newItem).attr("href", item.link)
                $(".recal_prod_price", newItem).text(item.price)
                $(".recal_prod_desc", newItem).html(item.description)
                $(".recal_prod_image img", newItem).attr("src", item.image)
                $(newItem).css("display", "block")
                $(newItem).removeClass("recal_product_template")
                $(currentrow).append(newItem)
                quickview.cdTriggerOnClick(newItem)
                i++;
            }
}

function populateStaticWithCategory(category) {
            var i = 1;
            for (var itemIndex in category.items) {
                /* Population predefined zones */
                var item = category.items[itemIndex];
                $(".recal_prod_title_" + i).text(item.title);
                $(".recal_prod_link_" + i).attr("href", item.link);
                $(".recal_prod_price_" + i).text(item.price);
                $(".recal_prod_shortdesc_" + i).html(item.shortdescription);
                $(".recal_prod_desc_" + i).html(item.description);
                $(".recal_prod_image_" + i).attr("src", item.image);
                i++;
            }
}
function populateAllCategories(categories) {
    for (var categoryIndex in categories) {
        var catObj = categories[categoryIndex]
        var category = $(".recal_category_template").clone()
        $(category).css("display", "block")
        $(category).removeClass("recal_category_template")
        $(category).addClass("recal_category_" + categoryIndex)
        $(category).addClass("recal_category")
        $(".recal_categories_container").append(category)
        populateCategory(catObj,categoryIndex)
    }
}
function showAllCategories() {
    $(".recal_category").css("display", "block")
}
function showOnlyCategory(catid) {
    $(".recal_category").css("display", "none")
    $(".recal_category_" + catid).css("display", "block")

}

function populateSideBar(boutique) {
    for (var categoryIndex in boutique.categories) {
        var category = boutique.categories[categoryIndex];
        $(".recal_sidebar .category_list").append('<li class="list__item"><a class="list__link recal_cat_selection" data-cat-id=' + categoryIndex + ' href="#">' + category.name + '</a></li>')
    }
    $(".recal_cat_selection").click(function () {
        var catid = $(this).attr('data-cat-id');
        //console.log(catid)
        if (catid == "all") {
            showAllCategories()
        } else {
            showOnlyCategory(catid)
        }
        return false;
    })
}
// Redefine the custom.js one
function mobileInteractionWithProduct(productContainer) {
    if (!productContainer) {
        return;
    }
    var products = document.querySelectorAll(productContainer);
    if (products.length === 0) {
        return;
    }
    if ( (window.innerWidth < 768) || (window.innerWidth < 768 && 'ontouchstart' in window) ) {
        for ( var i = 0, max = products.length; i < max; i++) {
            products[i].classList.add('product__control--hide');
            products[i].addEventListener('click', function() {
                var productLink = $('.product__control-left', $(this));
                window.location.href = productLink.attr("href");
            });
        }
    } else {
        if (products[0].classList.contains('product__control--hide')) {
            for ( var j = 0, max = products.length; j < max; j++) {
                products[j].classList.remove('product__control--hide');
            }
        }
    }
}
function populateAmazonShop(boutique) {
    populateStaticWithCategory(boutique.categories[0]);
    populateAllCategories(boutique.categories);
    populateSideBar(boutique);
    mobileInteractionWithProduct('.product.cd-item');
    quickview.initQuickView()
}